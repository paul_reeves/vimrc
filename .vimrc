" call pathogen#infect('~\vimfiles\bundle') 
"call pathogen#infect() 

set nocompatible
filetype off

" The only other things I use is eclim and that requires an installer
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'rstacruz/sparkup', {'rtp': 'vim'}
Bundle 'tpope/vim-fugitive'
Bundle 'msanders/snipmate.vim'
Bundle 'kchmck/vim-coffee-script'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/syntastic'
filetype plugin indent on     " required!
" Necesary  for lots of cool vim things

set showmode 

set number                      "Line numbers are good
set backspace=indent,eol,start  "Allow backspace in insert mode
set history=1000                "Store lots of :cmdline history
set showcmd                     "Show incomplete cmds down the bottom
set showmode                    "Show current mode down the bottom
set gcr=a:blinkon0              "Disable cursor blink
set visualbell                  "No sounds
set autoread                    "Reload files changed outside vim

" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden

"turn on syntax highlighting
syntax on

" ================ Search Settings  =================

set incsearch        "Find the next match as we type the search
set hlsearch         "Hilight searches by default
set viminfo='100,f1  "Save up to 100 marks, enable capital marks

:set guioptions-=m  "remove menu bar
:set guioptions-=T  "remove toolbar
:set guioptions-=r  "remove right-hand scroll bar

set autoindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

set  backspace=2

filetype plugin on
filetype indent on


" Custom leader 
let mapleader = ","


" Compile coffeescript files on save
au BufWritePost *.coffee silent CoffeeMake!
" folding by indentation use zi to toggle
au BufNewFile,BufReadPost *.coffee setl foldmethod=indent nofoldenable
" two space tabs
au BufNewFile,BufReadPost *.coffee setl shiftwidth=2 expandtab

" Map lnext and lprev (for syntastic) next and prev error
map <leader>ln :lnext<CR>
map <leader>lp :lprev<CR>


set guifont=Lucida_Console:h9:cANSI
colorscheme slate



" Ctrl-j/k deletes blank line below/above, and Alt-j/k inserts.
nnoremap <silent><C-j> m`:silent +g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <silent><C-k> m`:silent -g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <silent><A-j> :set paste<CR>m`o<Esc>``:set nopaste<CR>
nnoremap <silent><A-k> :set paste<CR>m`O<Esc>``:set nopaste<CR>


" Quickly open .vimrc file
nnoremap <leader>ev :vsplit $MYVIMRC<cr>


" This is totally awesome, exit insert mode with jk
inoremap jk <Esc>

" Space will toggle folds!
nnoremap <space> za


" Swap ; and :  Convenient.
nnoremap ; :
nnoremap : ;

" Stop the hightlighting after you are done with a search
nnoremap <esc> :noh<return><esc>

" make cursor move up one visible line rather than a logical one (more like a
" paragraph
nnoremap j gj
nnoremap k gk


" setting up some helper stuff. 
nnoremap <F3> :NERDTreeToggle<CR>
nnoremap <F12> :SyntasticToggleMode<CR>

" Attempts at getting eclim working
"autocmd  BufNewFile,BufReadPost, FileReadPre *.java call SetUpEclim()

fun! SetUpEclim()
  :echom "hello java world"
  :nnoremap <F2>  :echo 'Current time = ' . strftime('%c')<CR>
  :nnoremap <silent> <buffer> <leader>jp :ProjectProblems<cr>
  :nnoremap <leader>jg :JavaSearch<cr>
  
endfun

autocmd Filetype java call SetUpEclim()


